#!/usr/local/bin/perl
BEGIN
{
    use Test::More qw( no_plan );
    use_ok( 'Regexp::Common::Apache2' ) || BAIL_OUT( "Unable to load Regexp::Common::Apache2" );
    use lib './lib';
    use Regexp::Common qw( Apache2 );
    require( "./t/functions.pl" ) || BAIL_OUT( "Unable to find library \"functions.pl\"." );
};

my $tests = 
[
    {
        regex           => q{/John Doe/},
        regpattern      => q{John Doe},
        regsep          => q{/},
        test            => q{/John Doe/},
    },
    {
        regex           => q{m#John Doe#},
        regpattern      => q{John Doe},
        regsep          => q{#},
        test            => q{m#John Doe#},
    },
    {
        regex           => q{m$John Doe$},
        regpattern      => q{John Doe},
        regsep          => q{$},
        test            => q{m$John Doe$},
    },
    {
        regex           => q{m%John Doe%},
        regpattern      => q{John Doe},
        regsep          => q{%},
        test            => q{m%John Doe%},
    },
    {
        regex           => q{m^John Doe^},
        regpattern      => q{John Doe},
        regsep          => q{^},
        test            => q{m^John Doe^},
    },
    {
        regex           => q{m|John Doe|},
        regpattern      => q{John Doe},
        regsep          => q{|},
        test            => q{m|John Doe|},
    },
    {
        regex           => q{m?John Doe?},
        regpattern      => q{John Doe},
        regsep          => q{?},
        test            => q{m?John Doe?},
    },
    {
        regex           => q{m!John Doe!},
        regpattern      => q{John Doe},
        regsep          => q{!},
        test            => q{m!John Doe!},
    },
    {
        regex           => q{m'John Doe'},
        regpattern      => q{John Doe},
        regsep          => q{'},
        test            => q{m'John Doe'},
    },
    {
        regex           => q{m"John Doe"},
        regpattern      => q{John Doe},
        regsep          => q{"},
        test            => q{m"John Doe"},
    },
    {
        regex           => q{m,John Doe,},
        regpattern      => q{John Doe},
        regsep          => q{,},
        test            => q{m,John Doe,},
    },
    {
        regex           => q{m;John Doe;},
        regpattern      => q{John Doe},
        regsep          => q{;},
        test            => q{m;John Doe;},
    },
    {
        regex           => q{m:John Doe:},
        regpattern      => q{John Doe},
        regsep          => q{:},
        test            => q{m:John Doe:},
    },
    {
        regex           => q{m.John Doe.},
        regpattern      => q{John Doe},
        regsep          => q{.},
        test            => q{m.John Doe.},
    },
    {
        regex           => q{m_John Doe_},
        regpattern      => q{John Doe},
        regsep          => q{_},
        test            => q{m_John Doe_},
    },
    {
        regex           => q{m-John Doe-},
        regpattern      => q{John Doe},
        regsep          => q{-},
        test            => q{m-John Doe-},
    },
    {
        fail            => 1,
        name            => q{Illegal separaters},
        test            => q{*John Doe*},
    },
    {
        fail            => 1,
        name            => q{Unbalanced},
        test            => q{/John Doe#},
    },
    {
        fail            => 1,
        name            => q{false positive: /some/folder/file/here.txt},
        test            => q{/some/folder/file/here.txt},
    },
    {
        fail            => 1,
        name            => q{false positive: look mom/\w+/some/i},
        test            => q{look mom/\w+/some/i},
    },
    {
        fail            => 1,
        name            => q{false positive: Look-mom/Something/},
        test            => q{Look-mom/Something/},
    },
    {
        fail            => 1,
        name            => q{false positive: m/Error},
        test            => q{m/Error},
    },
    {
        fail            => 1,
        name            => q{false positive: /\w+/gas},
        test            => q{/\w+/gas},
    },
    {
        fail            => 1,
        name            => q{false positive: /\w+/gsair},
        test            => q{/\w+/gsair},
    },
    {
        name            => q{/\w+/gs;},
        regex           => q{/\w+/gs},
        regflags        => q{gs},
        regpattern      => q{\w+},
        regsep          => q{/},
        test            => q{/\w+/gs;},
        with_re_pos     => 1,
    },
    {
        name            => q{/\w+/},
        regex           => q{/\w+/},
        regpattern      => q{\w+},
        regsep          => q{/},
        test            => q{/\w+/},
    },
    {
        name            => q{m/\w+/},
        regex           => q{m/\w+/},
        regpattern      => q{\w+},
        regsep          => q{/},
        test            => q{m/\w+/},
    },
    {
        name            => q{/\w+/gs},
        regex           => q{/\w+/gs},
        regflags        => q{gs},
        regpattern      => q{\w+},
        regsep          => q{/},
        test            => q{/\w+/gs},
    },
    {
        name            => q{m/\w+/gs},
        regex           => q{m/\w+/gs},
        regflags        => q{gs},
        regpattern      => q{\w+},
        regsep          => q{/},
        test            => q{m/\w+/gs},
    },
    {
        name            => q{m,\w+,gs},
        regex           => q{m,\w+,gs},
        regflags        => q{gs},
        regpattern      => q{\w+},
        regsep          => q{,},
        test            => q{m,\w+,gs},
    },
    {
        fail            => 1,
        name            => q{//\w+/},
        test            => q{//\w+/},
    },
    {
        fail            => 1,
        name            => q{false positive: Look Mom!m/\w+/gs},
        test            => q{Look Mom!m/\w+/gs},
    },
    {
        name            => q{Look Mom m/\w+/gs},
        pos             => 9,
        regex           => q{m/\w+/gs},
        regflags        => q{gs},
        regpattern      => q{\w+},
        regsep          => q{/},
        test            => q{Look Mom m/\w+/gs},
        with_re_pos     => 1,
    },
    {
        name            => q{%{REDIRECT_FOO} =~ /bar/},
        pos             => 19,
        regex           => q{/bar/},
        regpattern      => q{bar},
        regsep          => q{/},
        test            => q{%{REDIRECT_FOO} =~ /bar/},
        with_re_pos     => 1,
    },
];

my $sub = $ENV{AUTHOR_TESTING} ? \&dump_tests : \&run_tests;
$sub->( $tests,
{
    type => 'Regexp',
    re => $RE{Apache2}{Regexp},
});
